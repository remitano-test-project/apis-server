import express from "express";
import { videoController } from "../controllers";
import { authMiddleware } from "../middleware";

const router = express.Router();

router.post('/share', authMiddleware, videoController.share);
router.get('/', videoController.getVideos);

export default router;