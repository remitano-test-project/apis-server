interface IResponse {
  status: number;
  message: string;
  data: any;
}
export class ResponseData implements IResponse {
  status: number;
  message: string;
  data: any;

  constructor(statusCode: number, message = "", data = {}) {
    this.status = statusCode;
    this.message = message;
    this.data = data;
  }
}
