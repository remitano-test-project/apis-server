import { Request } from "express"
export interface User {
  id?: string;
  email: string;
  password?: string;
}

export interface UserRegisterRequest {
  email: string;
  password: string;
}

export interface UserInfoResponse {
  code: number;
  message: string;
  data?: {
    email: string;
  };
}


export interface IAuthInfoRequest extends Request {
  user?: User
}
