module.exports = function (sequelize: any, Sequelize: any) {
  const Users = sequelize.define(
    "users",
    {
      id: {
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },

      email: {
        type: Sequelize.STRING,
      },

      password: {
        type: Sequelize.STRING,
      },
    },
    { createdAt: false, updatedAt: false }
  );

  return Users;
};
