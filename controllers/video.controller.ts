import {
  IAuthInfoRequest,
  Video,
  VideoCreateRequest,
  VideoQuerySearch,
  VideosInfoResponse,
  YoutubeVideoResponse,
} from "../types";
import { Request, Response } from "express";
import { Op } from "sequelize";
import { ResponseData } from "../utils";
import axios from "axios";

const { videos, sequelize } = require("../models");

const getInfoYoutubeVideo = async (videoUrl: string): Promise<Video> => {
  try {
    let result: Video = {
      url: "",
      owner: "",
      description: "",
      title: "",
    };
    const videoId = videoUrl.replace("https://www.youtube.com/watch?v=", "");
    const rs = (
      await axios(
        `https://www.googleapis.com/youtube/v3/videos?id=${videoId}&key=${process.env.YOUTUBE_DATA_API_KEY}&part=snippet`
      )
    ).data as YoutubeVideoResponse;
    if (rs.items.length) {
      result.description = rs.items[0].snippet.description;
      result.title = rs.items[0].snippet.title;
      result.url = videoUrl;
    }
    return result;
  } catch (error: any) {
    throw new Error(error);
  }
};

const share = async (req: IAuthInfoRequest, res: Response) => {
  try {
    const { url }: VideoCreateRequest = req.body;

    if (!url.match(/(https:\/\/www\.youtube\.com\/watch\?v=)\S+/gm)) {
      const response = new ResponseData(
        500,
        "Error: Video url was have wrong structure"
      );
      return res.status(500).send(response);
    }

    const videoIsExist = await videos.findOne({ where: { url } });

    if (videoIsExist) {
      const response = new ResponseData(500, "Error: Video was shared!");
      return res.status(500).send(response);
    }

    const videoYoutubeInfo = await getInfoYoutubeVideo(url);

    const newVideo: Video = {
      ...videoYoutubeInfo,
      owner: req.user?.email || "",
    };

    await videos.create(newVideo);
    const response = new ResponseData(200, "OK!", newVideo);
    res.status(200).send(response);
  } catch (error: any) {
    const response = new ResponseData(500, "Error", error);
    res.status(500).send(response);
  }
};

const getVideos = async (req: Request, res: Response) => {
  const LIMIT_ITEM = 10;
  try {
    const { page = 1 }: VideoQuerySearch = req.query;

    const videoList = await videos.findAndCountAll({
      where: {
        id: {
          [Op.between]: [(page - 1) * LIMIT_ITEM, page * LIMIT_ITEM],
        },
      },
    });

    const result : VideosInfoResponse = {
        data: videoList.rows,
        pagination: {
            page,
            total: videoList.count
        }
    }
    const response = new ResponseData(200, "", result);
    res.status(200).send(response);
  } catch (error: any) {
    const response = new ResponseData(500, "Error", error);
    res.status(500).send(response);
  }
};

export default {
  share,
  getVideos,
};
