export { default as authRouter } from "./auth.route";
export { default as videoRouter } from "./video.route";
export { default as userRouter } from "./user.route";
