import express, { Express, Request, Response } from "express";
import dotenv from "dotenv";
import cors from "cors";
dotenv.config();

const models = require("./models");
const routers = require("./routes");

const server: Express = express();
const port = process.env.PORT;

models.sequelize
  .sync()
  .then(function () {
    console.log("Connected DB");
  })
  .catch(function (err: any) {
    console.log(err, "Something went wrong with the Database Update!");
  });

server.use(express.json());
server.use(cors())

server.use("/auth", routers.authRouter);
server.use("/video", routers.videoRouter);
server.use("/user", routers.userRouter);

server.get("/", (req: Request, res: Response) => {
  res.send("Server is running");
});

server.listen(port, () => {
  console.log(`[server]: Server is running at http://localhost:${port}`);
});
