module.exports = function (sequelize: any, Sequelize: any) {
  const Video = sequelize.define(
    "videos",
    {
      id: {
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },

      title: {
        type: Sequelize.STRING,
      },

      url: {
        type: Sequelize.STRING,
      },

      description: {
        type: Sequelize.TEXT,
      },

      owner: {
        type: Sequelize.STRING,
      },
    },
    { createdAt: false, updatedAt: false }
  );

  return Video;
};
