import { NextFunction, Response } from "express";
import { ResponseData } from "../utils";
import jwt from "jsonwebtoken";
import { IAuthInfoRequest, User } from "../types";

export default function(req: IAuthInfoRequest, res: Response, next: NextFunction){
    const authHeader = req.headers.authorization;

    if (authHeader) {
        const token = authHeader.split(' ')[1];

        jwt.verify(token, process.env.SESSION_SECRET as string, (err, user) => {
            if (err) {
                const response = new ResponseData(403, '403 Forbidden Access is Denied.');
                return res.status(403).send(response);
            }

            req.user = user as User;
            next();
        });
    } else {
        const response = new ResponseData(401, 'Not authorized.');
        return res.status(401).send(response);
    }
};