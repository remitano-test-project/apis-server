export interface Video {
  url: string;
  owner: string;
  description: string;
  title: string;
}

export interface VideoCreateRequest {
  url: string;
}

export interface VideoQuerySearch {
  page?: number;
}

export interface YoutubeVideoInfo {
  kind: string;
  etag: string;
  id: string;
  snippet: {
    title: string;
    description: string;
  };
}

export interface YoutubeVideoResponse {
  kind: string;
  etag: string;
  items: YoutubeVideoInfo[];
}

export interface VideosInfoResponse {
  data: Video[];
  pagination: {
    page: number;
    total: number;
  };
}
