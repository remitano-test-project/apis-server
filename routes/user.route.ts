import express from "express";
import { userController } from "../controllers";
import { authMiddleware } from "../middleware";

const router = express.Router();

router.get('/profile', authMiddleware, userController.getUserInfo);

export default router;