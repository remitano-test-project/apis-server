import { IAuthInfoRequest, User, UserRegisterRequest } from "../types";
import { Request, Response } from "express";
import { ResponseData } from "../utils";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";

const { users } = require("../models");

const register = async (req: Request, res: Response) => {
  try {
    const { email, password }: UserRegisterRequest = req.body;

    console.log({ email, password });
    const hash_password = bcrypt.hashSync(password, 10);
    const userIsExist = await users.findOne({ where: { email } });

    if (userIsExist) {
      const response = new ResponseData(500, "Error: User was exist!");
      return res.status(500).send(response);
    }

    const newUser: User = {
      email,
      password: hash_password,
    };

    const user = await users.create(newUser);

    const signData = {
      id: user.id,
      email: user.email,
    };

    const access_token = jwt.sign(
      signData,
      process.env.SESSION_SECRET as string
    );

    res.status(200).send({
      result: {
        email: user.email,
        login_result: {
          access_token,
          user_info: {
            email: user.email,
          },
        },
      },
      error: {
        code: null,
        message: "",
      },
    });
  } catch (error: any) {
    const response = new ResponseData(500, "Error", error);
    res.status(500).send(response);
  }
};

const login = async (req: Request, res: Response) => {
  try {
    const { email, password = "" }: User = req.body;

    const user = await users.findOne({ where: { email } });

    if (!user) {
      const response = new ResponseData(500, "User not exist!");
      return res.status(500).send(response);
    }

    if (!bcrypt.compareSync(password, user.password)) {
      const response = new ResponseData(
        500,
        "Username or password wrong! Try again."
      );
      return res.status(500).send(response);
    }

    const signData = {
      id: user.id,
      email: user.email,
    };

    const access_token = jwt.sign(
      signData,
      process.env.SESSION_SECRET as string
    );
    res.status(200).send({
      result: {
        access_token,
        user_info: {
          email: user.email,
        },
      },
      error: {
        code: null,
        message: "",
      },
    });
  } catch (error: any) {
    const response = new ResponseData(500, "Error", error);
    res.status(500).send(response);
  }
};

const getUserInfo = async (req: IAuthInfoRequest, res: Response) => {
  try {
    const email = req.user?.email;

    const user = await users.findOne({ where: { email } });

    if (!user) {
      const response = new ResponseData(500, "User not exist!");
      return res.status(500).send(response);
    }

    res.status(200).send({
      result: {
        email: user.email,
      },
      error: {
        code: null,
        message: "",
      },
    });
  } catch (error: any) {
    const response = new ResponseData(500, "Error", error);
    res.status(500).send(response);
  }
};

export default {
  login,
  register,
  getUserInfo,
};
